#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct box { int x, y, z; };

#define FITS(X, Y, Z) (b1->X < b2->x && b1->Y < b2->y && b1->Z < b2->z)

bool box_fits(structrbox *b1, struct box *b2) {
	return FITS(x, y, z) || FITS(x, z, y) ||
		FITS(y, x, z) || FITS(y, z, x) ||
		FITS(z, x, y) || FITS(z, y, x);
}

int n_boxes(struct box *box, int *memo, int n) {
	
}

int main() {
	int n;

	scanf("%d", &n);

	struct box[n];
	int n_boxes[n];

	for (int i = 0; i < n; i++) {
		n_boxes[i] = 0;
		scanf("%d%d%d", &box[i].x, &box[i].y, &box[i].z);
	}
	
	return 0;
}
